# My Spacemacs Configuration

[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://github.com/syl20bnr/spacemacs)
 
## About

This is my Spacemacs configuration. In order to use it just clone this repo to your home directory. Make sure there's no `.spacemacs` file in your home folder, otherwise this configuration won't be loaded.

## Dependencies

I recommend you install silversearcher-ag. You will have to run php-extras-generate-eldoc if you plan to use `php-mode`.

## Layers

This configuration will download packages for the following languages:

 * PHP
 * Javascript
 * Python
 * Go
 * Nim
 * Sass
 
Blade templates are opened in `web-mode`. I've also added `vue-mode` to support Vue.js single file components. Additional support for Dockerfiles is also included.
 
## Additional commands

### General

 * `SPC o s` invokes `just-one-space`.
 * `SPC o l` copies the whole line.
 * `C-c C-k` does the same, but also works on the mini buffer.
 * `C-+` and `C--` will increase and decrease font size.
 * `SPC o E` will escape HTML tags in the selected region.
 * `SPC o p` will call `ag-project`.
 * `SPC o k` will kill all search buffers (`ag-kill-buffers`).
 
### Markdown

 * `SPC o t` will invoke `markdown-toc-generate-toc`.

### Org

 * `SPC o c` will invoke `org-capture`.
 * `SPC o C` will invoke `org-cut-subtree`.
 * `SPC o o` will copy the link under the cursor.
 
## Spacebadges

A little extension of mine that inserts Spacemacs badges in Markdown, Org and HTML modes. To insert a badge use the following commands:

 * `SPC o b o` for org-mode.
 * `SPC o b m` for markdown-mode.
 * `SPC o b w` for web-mode.
